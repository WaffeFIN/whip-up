[App](https://whip-up.firebaseapp.com)

[Firebase Console](https://console.firebase.google.com)

[Bookkeeping](https://docs.google.com/spreadsheets/d/1d4_RxOghKDLQb0jAVhEw5DClJ2bBuF3AocItKsXXGW0/edit?usp=sharing)

### What is what ###

We might end up using plenty of libraries/stuff, so to keep up with what they do, we summarize them here:

* React: open-source JavaScript library for building user interfaces.
* [Document Object Model](https://www.w3.org/TR/DOM-Level-2-Core/introduction.html): With DOM, programmers can build documents, navigate their structure, and add, modify, or delete elements and content
* ReactDOM: the DOM in react
* JavaScript: the programming language cmon
* Babel: JS compiler. "Put in next-gen JavaScript, Get browser-compatible JavaScript out"
* webpack: Bundler. Takes modules with dependencies and emits static assets representing those modules.
    * "With Webpack you tell it which transformations to make on your code, while Babel is the specific transformation itself"
* Firebase: Mobile and web application development platform. Our choice of DB

# Whip-Up #
### What is Whip-Up? ###

Whip-Up is a webapp for everyone who loves to cook and to try new delicious food experiences. Whip-Up allows you to effortlessly create all the necessary shopping lists you need for you and your family. Feeling adventurous? Dive into the RecipeBrowser and discover your new favorite dish! Have you whipped up a great meal? Create your own recipes and share them with the world!

Here are some of the key features of Whip-Up:

* Create your grocery lists online! That way you'll never forget them back home when out and shopping.
* Browse delicious recipes and add them to your shopping list with a click of a button
* Rate your favorite dishes and share your own recipes with the world!

### Installation instructions ###
Download the source code from this repo, initialize with "npm install" and "npm start", enjoy! Easy as pie! There are no tests, but in case you'd like to test it out for yourself, we recommend you to use "npm test" after making your own test files.

"Tests are for the weak" - Charlemange 1823

### User manual ###
Use the links on the site, it's really as easy as boiling water.

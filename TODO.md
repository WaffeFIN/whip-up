### Review ###

* Recipe rating
* Ingredients can't have zero or negative amount
    * Remove these ingredients
* Clean up database
* Foodgroup sorting default value (always lowest)
* Write installation instructions
* Double check all project submission requirements
* Check app looks with phone
    * Quite OK actually :)
* Foodgroup selection menu
* Split ingredients to amount and ingredient name for nicer A E S T H E T I C S

### Working ###

* Continue on CSS
    * Center shoppinglist list
    * Sorting labels
    * Recipe: ingredients to right side (use row)
    * Foodproducts: columnify adder
    * Center ingredient list
    * Boxify ingredients (table)

### TODO ###


### Postponed ###

* Add foodgroup submenus
* Display message when saving values or when error occurs (make message as a component)
* Prettify links if possible?
    * Redirect from .../76JEHhTJ, to .../something?id=76JEHhTJ
* Futher refactor code
* Add proptypes to the most important parts (near database queries etc.)
* Fix tests
* Add tests
* Upload picture for recipe
    * Saved in cloud services
* Rethink db structure... foodgroups in separate object
* Users
* Autocreate shopping list from recipebrowser
* User preferences
* Fridge/storage of already owned ingredients
* Merge shopping lists
* Already owned ingredients are removed from shopping list (on display)
* Change recipe instructions to steps
* Add a new feature to follow making a recipe, always showing current and next step from the instructions.
* Plan and save daily meals with recipes and/or ingredients
* Generate shopping lists for an interval of time

### Thoughts/Doodles ###

* check webpack dev server
* check proptypes

### Completed ###

Place the completed task always on top of list:

* _Aborted: Write user manual_
* Footer
* Hardcode foodgroup "sorting value" and sort by them. Use Arabia S-Market order
* Sort recipes by name, rating or effort
* Add foodgroup sorting value to db 
* Fix app looks (CSS)
* Autoadd ingredients from recipe (in shoppinglist)
* Minor: Include picture in recipe link (link is from recipebrowser to recipe)
* Refactor code (subdivide into smaller components)
* Ingredient stacking
* Fix save overwrite
* Get measure when displaying ingredients (default: g)
* _Aborted: Change /list to blank (new) list_
* On save redirect to saved list page (with ID)
* Recipe components
* Write and save recipes using ingredients
* Display recipe 
* Write description
* _Aborted: Save shopping list redirects to shoppinglist page (with ID in URL)_
* Display shopping lists
* Change "shoppinglist" URLs to "list"
* Save shopping list (publicly)
* Use ingredients "branch" when saving shoppinglists
* Sort shopping list according to food groups
* Start using database in app
* Adding food products to db via app
* Can use server-side food products when creating shopping list
* Shopping list from list of strings to list of ingredient-objects
* Fix shopping list add
* Initialize test data at database
* Main page
* Write own shopping list
* Display shopping list
	* Shopping list component
* _Aborted: Authentication_

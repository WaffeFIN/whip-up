import React, { Component } from 'react';

var NavLink = require('react-router-dom').NavLink;

class Nav extends Component {
  render (){
  	return (
      <nav>
        <ul className='navbar'>
          <li>
            <NavLink exact activeClassName='active' to='/'>
              Home
            </NavLink>
          </li>
          <li>
            <NavLink activeClassName='active' to='/list'>
              List
            </NavLink>
          </li>
          <li>
            <NavLink activeClassName='active' to='/recipes'>
              Recipes
            </NavLink>
          </li>
          <li>
            <NavLink activeClassName='active' to='/foodproduct'>
              Food Products
            </NavLink>
          </li>
        </ul>
      </nav>
    )
  }
}

export default Nav;
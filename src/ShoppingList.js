import React, { Component } from 'react';
import update from 'react-addons-update';
import database from './index';
import * as Ingredients from './IngredientHelpers';


var queryString = require('query-string');
var randomize = require('randomatic');
var base64 = require('base-64');
var Link = require('react-router-dom').Link;
import './App.css';


class ShoppingList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ingredients: null,
      foodproducts: null,
      foodproductdb: null,
      addedrecipes:null,
      recipes: null,
      foodgroups: null,
      edited: false,
      error: null
    }

    this.submitIngredient = this.submitIngredient.bind(this);
    this.submitRecipe = this.submitRecipe.bind(this);
    this.fetchFoodProducts = this.fetchFoodProducts.bind(this);
    this.fetchRecipes = this.fetchRecipes.bind(this);
    this.fetchIngredients = this.fetchIngredients.bind(this);
    this.fetchFoodGroups = this.fetchFoodGroups.bind(this);
    this.saveNewList = this.saveNewList.bind(this);
  }

  submitIngredient(ingredients) {
    if (!this.state.ingredients) {
      var newArray = null;
    } else {
      var newArray = this.state.ingredients.slice();      
    }
    ingredients.map(function (ingredient) {
      var newIngredient = ingredient;
      if (!newIngredient.name) {
        newIngredient.name = ingredient.foodproduct.name;
      }
      if (!newArray || newArray.length === 0) {
        newArray = [newIngredient];
      } else {
        for (var i = 0; i < newArray.length; i++) {
          var listIngredient = newArray[i];
          if (listIngredient.name === newIngredient.name) {
            listIngredient.amount = Number(listIngredient.amount) + Number(newIngredient.amount);  
            if (listIngredient.amount > 0) {
              newArray[i] = listIngredient;
            } else if (newArray.length > 1) {
              newArray.splice(i, 1);
            } else {
              newArray = [];     
            }
            break;
          } else if (i === (newArray.length-1)) {
            newArray.push(newIngredient);
            
            const foodproducts = this.state.foodproductdb;
            const foodgroups = this.state.foodgroups;
            newArray.sort(function(a,b) {
              let aval = foodgroups[foodproducts[a.name].foodgroup];
              let bval = foodgroups[foodproducts[b.name].foodgroup];

              if (!aval) aval = Infinity;
              if (!bval) bval = Infinity;

              return ((aval < bval) ? -1 : 
                     ((aval > bval) ?  1 : 0));
            })

            break;
          }
        }
      }
    }.bind(this));
    this.setState(function () {
      return {
        ingredients: newArray,
        edited: true
      }
    })  
  }

  submitRecipe (addedrecipe) {
    var newRecipeAdd = addedrecipe;
    newRecipeAdd.id = addedrecipe.recipe.id;
    
    var ingredients = addedrecipe.recipe.ingredients;
    var multiplier = addedrecipe.multiplier;

    var ingredientList = [];

    Object.keys(ingredients).map(function (key,index) {
      let ingredient = {};
      ingredient.name = key;
      ingredient.amount = Number(ingredients[key])*multiplier;

      ingredientList.push(ingredient);
    });

    this.submitIngredient(ingredientList);

    if (!this.state.addedrecipes || this.state.addedrecipes.length === 0) {
      this.setState(function () {
        return {
          addedrecipes: [addedrecipe]
        } 
      })
    } else {
      var newArray = this.state.addedrecipes.slice();

      for (var i = 0; i < newArray.length; i++) {
        var listedRecipe = newArray[i];
        if (listedRecipe.id === newRecipeAdd.id) {
          listedRecipe.multiplier = Number(listedRecipe.multiplier) + Number(newRecipeAdd.multiplier);          
          if (listedRecipe.multiplier > 0) {
            newArray[i] = listedRecipe;
          } else if (newArray.length > 1) {
            newArray.splice(i, 1);
          } else {
            newArray = [];       
          }
          break;
        } else if (i === (newArray.length-1)) {
          newArray.push(newRecipeAdd);
          break;
        }
      } 
      this.setState(function () {
        return {
          addedrecipes: newArray
        }
      })
    }
    this.setState(function () {
      return {
        edited: true
      }
    })  
  }


  fetchFoodGroups() {
    database.ref('/foodgroups-sorting').once('value').then(function (foodgroups) {
      return foodgroups.val();
    }).then(function (foodgroups) {
      this.setState(function () {
        return {
          foodgroups: foodgroups
        }
      })
    }.bind(this));
  }


  fetchFoodProducts() {
    database.ref('/foodproducts').once('value').then(function (foodproducts) {
      return foodproducts.val();
    }).then(function (foodproducts) {
      var foodproductList = [];
      Object.keys(foodproducts).map(function (key,index) {
        let foodproduct = foodproducts[key];
        foodproduct.name = key;
        foodproductList.push(foodproduct);
      })

      const foodgroups = this.state.foodgroups;
      foodproductList.sort(function(a,b) {
        return ((foodgroups[a.foodgroup] < foodgroups[b.foodgroup]) ? -1 : 
                ((foodgroups[a.foodgroup] > foodgroups[b.foodgroup]) ? 1 : 0));
      })

      this.setState(function () {
        return {
          foodproducts: foodproductList,
          foodproductdb: foodproducts
        }
      })
    }.bind(this));
  }


  fetchRecipes() {
    database.ref('/recipes').once('value').then(function (recipes) {
      return recipes.val();
    }).then(function (recipes) {
      var recipeList = [];
      Object.keys(recipes).map(function (key,index) {
        let recipe = recipes[key];
        recipe.id = key;
        recipeList.push(recipe);
      })
      this.setState(function () {
        return {
          recipes: recipeList
        }
      })
    }.bind(this));
  }
  

  fetchIngredients(id) {
    database.ref('/shoppinglists/' + id + '/ingredients').once('value').then(function (ingredients) {
      return ingredients.val();
    }).then(function (ingredients) {
      if (!ingredients) {
        this.setState(function () {
          return {
            error: 'Looks like there was an error. Check that you are searching for an existing list.',
          }
        })
      } else {
        var ingredientList = [];
        Object.keys(ingredients).map(function (key,index) {
          let ingredient = {};
          ingredient.name = key;
          ingredient.amount = ingredients[key];
          ingredientList.push(ingredient);
        })

        const foodproducts = this.state.foodproductdb;
        const foodgroups = this.state.foodgroups;
        ingredientList.sort(function(a,b) {
          return ((foodgroups[foodproducts[a.name].foodgroup] < foodgroups[foodproducts[b.name].foodgroup]) ? -1 : 
                  ((foodgroups[foodproducts[a.name].foodgroup] > foodgroups[foodproducts[b.name].foodgroup]) ? 1 : 0));
        })

        this.setState(function () {
          return {
            ingredients: ingredientList
          }
        })
      }
    }.bind(this));
  }

  fetchAddedRecipes(id) {
    database.ref('/shoppinglists/' + id + '/recipes').once('value').then(function (recipes) {
      return recipes.val();
    }).then(function (recipes) {
      if (!recipes) {
        this.setState(function () {
          return {
            error: 'Looks like there was an error. Check that you are searching for an existing list.',
          }
        })
      } else {
        var recipeList = [];
        Object.keys(recipes).map(function (key,index) {
          let recipe = {};
          recipe.id = key;
          recipe.multiplier = recipes[key].multiplier;
          recipe.name = recipes[key].name;
          recipeList.push(recipe);
        })
//------------------- SORT HERE -----------------------------//
        this.setState(function () {
          return {
            addedrecipes: recipeList
          }
        })
      }
    }.bind(this));
  }


  saveNewList(name) {
    var id = null;
    database.ref('/shoppinglists').once('value').then(function (shoppinglists) {
      while (true) {
        id = base64.encode(randomize('*', 6));

        if (!shoppinglists[id]) {
          break;
        }
      }
    }).then(function () {
      var list = {};
      this.state.ingredients.map(function(ingredient) {
        list[ingredient.name] = ingredient.amount;
      })
      var rlist = {};

      database.ref('/shoppinglists/' + id + '/name').set(name);
      database.ref('/shoppinglists/' + id + '/ingredients').set(list);

      if (this.state.addedrecipes) {
        this.state.addedrecipes.map(function(addedrecipe) {
          rlist[addedrecipe.name] = addedrecipe.multiplier;
        })

        database.ref('/shoppinglists/' + id + '/recipes').set(list);
      }


      if (this.props.newlist) {
        this.props.historyToUse.push('/list/edit?id=' + id);
      } else {
        this.props.history.push('/list/edit?id=' + id);
      }
    }.bind(this));
  }



  componentDidMount() {
    this.fetchFoodGroups();
    this.fetchRecipes();
    this.fetchFoodProducts();

    if (!this.props.newlist) {
      var id = queryString.parse(this.props.location.search).id;

      database.ref('/shoppinglists/' + id).once('value').then(function (shoppinglist) {
        return shoppinglist.val();
      }).then(function (shoppinglist) {
        if (shoppinglist === null) {
          return this.setState(function () {
            return {
              error: 'Looks like there was an error. Check that you are searching for an existing list.',
            }
          });
        }

        this.fetchIngredients(id);

        this.setState(function () {
          return {
            name: shoppinglist.name,
            error: null
          }
        });
      }.bind(this));
    }
  }

  render() {
    if (this.state.error) {
      return (
        <div className='alert error'>
          <p>{this.state.error}</p>
          <Link to='/list'>Back to lists</Link>
        </div>
      )
    }


    return (
      <div className='shoppinglist'>
        <h3>Shopping list</h3>
        {(this.state.ingredients && this.state.foodproductdb) &&
          <Ingredients.IngredientList foodproducts={this.state.foodproductdb} ingredients={this.state.ingredients}/>
        }
        {this.state.foodproducts &&
          <Ingredients.IngredientAdd foodproducts={this.state.foodproducts} onSubmit={this.submitIngredient}/>
        }
        {this.state.addedrecipes &&
          <Ingredients.RecipeList recipes={this.state.addedrecipes}/>
        }
        {this.state.recipes &&
          <Ingredients.RecipeAdd recipes={this.state.recipes} onSubmit={this.submitRecipe}/>
        }
        {this.state.edited &&
          <SaveList save={this.saveNewList} name={this.state.name}/>
        }
      </div>
    )
  }
}

class SaveList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listname: ''
    }

    this.handleListNameChange = this.handleListNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleListNameChange(event) {
    var value = event.target.value;

    this.setState(function(){
      return {
        listname: update(this.state.listname, {$set: value})
      }
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.save(this.state.listname);

    this.setState(function(){
      return {
        listname: ''
      }
    })
  }

  render() {
    return (
      <form className='row' onSubmit={this.handleSubmit}>
        <input
          id='listName'
          placeholder='Name of list (optional)'
          type='text'
          autoComplete='off'
          value={this.state.listname}
          onChange={this.handleListNameChange}/> 
        <button
          className='button'
          type='submit'>
        Save
        </button>
      </form> 
    )
  }
}



export default ShoppingList;

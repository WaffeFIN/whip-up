import React, { Component } from 'react';

var Link = require('react-router-dom').Link;

class Home extends Component {
  render() {
    return (
      <div className='home'>
        <h1 className='home-header'>Whip-Up</h1>
        <img src='cake.svg' className='Home-image' alt='Home' />
        <div className='home-menu'>
          <MenuButton text='Check your shopping list' img='list.svg' to='/list' />
          <MenuButton text='Browse recipes' img='dish.svg' to='/recipes' />
        </div>
      </div>
    )
  }
}

class MenuButton extends Component {
  render() {
    return (
      <Link className='button' to={this.props.to}>
        <div className="home-button">
          <img src={this.props.img} className="MenuButton-image" alt={this.props.text} />
          <p className="Menubutton-text">
            {this.props.text}
          </p>
        </div>
      </Link>
    )
  }
}

export default Home;
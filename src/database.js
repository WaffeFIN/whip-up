import database from './index';

function fetchFoodProducts() {
	return database.ref('/foodproducts').once('value').then(function (foodproducts) {
      return foodproducts.val();
    })
}

function fetchFoodGroups() {
	return database.ref('/foodgroups').once('value').then(function (foodgroups) {
      return foodgroups.val();
    })
}

function fetchInstructions() {
	return database.ref('/instructions').once('value').then(function (instructions) {
      return instructions.val();
    })
}

function fetchRecipes() {
	return database.ref('/recipes').once('value').then(function (recipes) {
      return recipes.val();
    })
}

function fetchShoppingLists() {
	return database.ref('/shoppinglists').once('value').then(function (shoppinglists) {
      return shoppinglists.val();
    })
}


export default fetchFoodProducts;
export {fetchFoodGroups,fetchInstructions,fetchRecipes,fetchShoppingLists};
import React, { Component } from 'react';
import update from 'react-addons-update';
import fetchFoodProducts from './database';
import database from './index';
import './App.css';


class FoodProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      foodproducts: null,
      foodgroups: null
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateFoodProductList = this.updateFoodProductList.bind(this);
    this.fetchFoodGroups;
  }

  handleSubmit(foodproduct) {
    database.ref('foodproducts/' + foodproduct.name).set({
      foodgroup: foodproduct.foodgroup
    });
    this.updateFoodProductList();
  }

  componentDidMount() {
    this.fetchFoodGroups();
    this.updateFoodProductList();
  }

  updateFoodProductList() {
    fetchFoodProducts().then(function (foodproducts) {
      var foodproductList = [];
      Object.keys(foodproducts).map(function (key,index) {
        let foodproduct = foodproducts[key];
        foodproduct.name = key;
        foodproductList.push(foodproduct);
      })

      const foodgroups = this.state.foodgroups;
      
      foodproductList.sort(function(a,b) {
        let aval = foodgroups[foodproducts[a.name].foodgroup];
        let bval = foodgroups[foodproducts[b.name].foodgroup];

        if (!aval) aval = Infinity;
        if (!bval) bval = Infinity;

        return ((aval < bval) ? -1 : 
               ((aval > bval) ?  1 : 0));
      })

      this.setState(function () {
        return {
          foodproducts: foodproductList
        }
      })
    }.bind(this));
  }

  fetchFoodGroups() {
    database.ref('/foodgroups-sorting').once('value').then(function (foodgroups) {
      return foodgroups.val();
    }).then(function (foodgroups) {
      this.setState(function () {
        return {
          foodgroups: foodgroups
        }
      })
    }.bind(this));
  }

  render() {
    var fg = [];
    if (this.state.foodgroups) {
      fg = [...Object.keys(this.state.foodgroups)];
    }
    return (
      <div className='foodproduct'>
        {this.state.foodgroups &&
          <AddFoodProduct onSubmit={this.handleSubmit} foodgroups={fg} />
        }
        {this.state.foodproducts &&
          <FoodProductList foodproducts={this.state.foodproducts}/>
        }
      </div>
    )
  }
}

function FoodProductList (props) {
  return (
    <div className='foodproduct-list'>
      <h3>Food products available</h3>
      <ul>
        {props.foodproducts.map(function (product){
          return <li>{product.name}</li>;  
        })}
      </ul>
    </div>
  )
}

class AddFoodProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: null,
      measure: null,
      foodgroup: null      
    }

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleMeasureChange = this.handleMeasureChange.bind(this);
    this.handleFoodGroupChange = this.handleFoodGroupChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    var value = event.target.value;

    this.setState(function(){
      return {
        name: value
      }
    })
  }

  handleMeasureChange(event) {
    var value = event.target.value;

    this.setState(function(){
      return {
        measure: value
      }
    })
  }

  handleFoodGroupChange(event) {
    var value = event.target.value;

    this.setState(function(){
      return {
        foodgroup: value
      }
    })
  }

  handleSubmit(event) {
    event.preventDefault();

    var newFoodProduct = {};
    newFoodProduct.name = this.state.name;
    newFoodProduct.foodgroup = this.state.foodgroup;
    if (!(this.state.measure === 'g' || this.state.measure === null)) {
      newFoodProduct.measure = this.state.measure;
    }

    this.props.onSubmit(newFoodProduct);

    this.setState(function(){
      return {
        name: null,
        measure: null,
        foodgroup: null
      }
    })
  }

  render() {
    return (
      <form className='row' onSubmit={this.handleSubmit}>
        <input
          id='foodproduct'
          placeholder='Product'
          type='text'
          autoComplete='off'
          value={this.state.name || ''}
          onChange={this.handleNameChange}/>
        <input
          id='measure'
          placeholder='Measure type'
          type='text'
          autoComplete='off'
          value={this.state.measure || ''}
          onChange={this.handleMeasureChange}/>
        <select id='foodgroup' onChange={this.handleFoodGroupChange}>
          {this.props.foodgroups.map(function (foodgroup, index){
            return <option value={foodgroup}>{foodgroup}</option>;  
          })}
        </select>
        <button
          className='button'
          type='submit'
          disabled={!(this.state.name && this.state.foodgroup)}>
        Add food product
        </button>
      </form>
    )
  }
}




export default FoodProduct;


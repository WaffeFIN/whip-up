import React, { Component } from 'react';
import database from './index';
import {IngredientList} from './IngredientHelpers';
import Icon from './Icon';

var Stars = require('react-rating');
var queryString = require('query-string');
var Link = require('react-router-dom').Link;
import './App.css';



class Recipe extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      previousRate: null,
      recipe: null,
      ingredients: null,
      instructions: null,
      foodproducts: null,
      foodgroups: null,
      error: null
    }

    this.fetchRecipe = this.fetchRecipe.bind(this);
    this.listIngredients = this.listIngredients.bind(this);
    this.fetchInstructions = this.fetchInstructions.bind(this);
    this.fetchFoodProducts = this.fetchFoodProducts.bind(this);
    this.fetchFoodGroups = this.fetchFoodGroups.bind(this);
    this.getScore = this.getScore.bind(this);
    this.handleRate = this.handleRate.bind(this);
  }

  fetchRecipe(id) {
    database.ref('/recipes/' + id).once('value').then(function (recipe) {
      return recipe.val();
    }).then(function (recipe) {
      if (recipe === null) {
        return this.setState(function () {
          return {
            error: 'Looks like there was an error. Check that you are searching for an existing recipe.',
          }
        });
      }

      this.listIngredients(id);
      this.fetchInstructions(id);      

      this.setState(function () {
        return {
          recipe: recipe,
          error: null
        }
      });
    }.bind(this));
  }

  listIngredients(id) {
    database.ref('/recipes/' + id + '/ingredients').once('value').then(function (ingredients) {
      return ingredients.val();
    }).then(function (ingredients) {
      var ingredientList = [];
      Object.keys(ingredients).map(function (key,index) {
        let ingredient = {};
        ingredient.name = key;
        ingredient.amount = ingredients[key];
        ingredientList.push(ingredient);
      })

      const foodproducts = this.state.foodproducts;
      const foodgroups = this.state.foodgroups;
      
      ingredientList.sort(function(a,b) {
        return ((foodgroups[foodproducts[a.name].foodgroup] < foodgroups[foodproducts[b.name].foodgroup]) ? -1 : 
                ((foodgroups[foodproducts[a.name].foodgroup] > foodgroups[foodproducts[b.name].foodgroup]) ? 1 : 0));
      })

      this.setState(function () {
        return {
          ingredients: ingredientList
        }
      })
    }.bind(this));
  }

  fetchInstructions(id) {
    database.ref('/instructions/' + id).once('value').then(function (instructions) {
      return instructions.val();
    }).then(function (instructions) {
      this.setState(function () {
        return {
          instructions: instructions
        }
      });
    }.bind(this));
  }

  fetchFoodGroups() {
    database.ref('/foodgroups-sorting').once('value').then(function (foodgroups) {
      return foodgroups.val();
    }).then(function (foodgroups) {
      this.setState(function () {
        return {
          foodgroups: foodgroups
        }
      })
    }.bind(this));
  }

  fetchFoodProducts() {
    database.ref('/foodproducts').once('value').then(function (foodproducts) {
      return foodproducts.val();
    }).then(function (foodproducts) {
      this.setState(function () {
        return {
          foodproducts: foodproducts
        }
      })
    }.bind(this));
  }

  componentDidMount() {
    this.fetchFoodGroups();
    this.fetchFoodProducts();

    var id = queryString.parse(this.props.location.search).id;
    this.setState({
      id: id
    })
    this.fetchRecipe(id);
  }

  getScore(stars) {
    var votes = 0;
    var starcount = 0;

    Object.keys(stars).map(function (key,index) {
      votes += stars[key];
      starcount += (Number(key)*stars[key]);
    })

    if (votes != 0) {
      starcount /= votes;
      var score = starcount.toFixed(1);
    }
    return score;
  }

  handleRate(rate, event) {
    var stars = rate;
    if (this.state.previousRate) {
      let previousStars = this.state.previousRate;
      database.ref('/recipes/'+ this.state.id +'/stars/' + previousStars).set(this.state.recipe.stars[previousStars]);
    }
    database.ref('/recipes/'+ this.state.id +'/stars/' + stars).set(this.state.recipe.stars[stars] + 1);
    this.setState({
      previousRate: stars,
    })
  }

  render() {
    if (this.state.error) {
      return (
        <div className = 'alert error'>
          <p>{this.state.error}</p>
          <Link to='/recipes'>Back to recipes</Link>
        </div>
      )
    }

    return (
      <div className='recipe-full'>
        {this.state.recipe &&
          <RecipeInfo recipe={this.state.recipe} score={(this.state.previousRate ? this.state.previousRate : this.getScore(this.state.recipe.stars))}
                      onRate={this.handleRate} instructions={this.state.instructions}
          />
        }
        {(this.state.ingredients && this.state.foodproducts) &&
          <IngredientList foodproducts={this.state.foodproducts} ingredients={this.state.ingredients}/>
        }
      </div>
    )
  }
}

function RecipeInfo (props) {
  return (
    <div className='recipe-info'>
      <h2>{props.recipe.name}</h2>
      <Stars stop={4} initialRate={+props.score} onClick={props.onRate} 
             empty={<Icon src='star_empty.svg' alt='empty star'/>}
             placeholder={<Icon src='star_full.svg' alt='star'/>}
             full={<Icon src='star_full.svg' alt='star'/>}
      />
      <p>Effort: {props.recipe.effort} min</p>
      <h3>Instructions</h3>
      <p>{props.instructions}</p>         
    </div>
  )
}


export default Recipe;
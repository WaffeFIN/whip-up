import React, { Component } from 'react';
import database from './index';
import ShoppingList from './ShoppingList'

var randomize = require('randomatic');
var base64 = require('base-64');
var Link = require('react-router-dom').Link;

import './App.css';



class ShoppingListBrowser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shoppinglists: null
    }

    this.updateShoppingLists = this.updateShoppingLists.bind(this);
  }

  updateShoppingLists() {
    database.ref('/shoppinglists').once('value').then(function (shoppinglists) {
      return shoppinglists.val();
    }).then(function (shoppinglists) {
      var listOfLists = [];
      Object.keys(shoppinglists).map(function (key,index) {
        let shoppingList = shoppinglists[key];
        shoppingList.id = key;
        listOfLists.push(shoppingList);
      })

      this.setState(function () {
        return {
          shoppinglists: listOfLists
        }
      })
    }.bind(this));
  }

  componentDidMount() {
    this.updateShoppingLists();
  }

  render() {
    return (
      <div className='shoppinglist-browser'>
        <ShoppingList newlist={true} historyToUse={this.props.history}/>
        
        <h3>Existing shoppinglists:</h3>
        {this.state.shoppinglists &&
          <SelectShoppingList match={this.props.match} shoppinglists={this.state.shoppinglists}/>
        }
      </div>
    )
  }
}



function SelectShoppingList (props) {
  return (
    <div className='shoppinglist-list'>
      <ul>
        {props.shoppinglists.map(function (shoppinglist) {
          return (
            <li>
              <Link
                className='button'
                to={{
                  pathname: props.match.url + '/edit',
                  search: '?id=' + shoppinglist.id
                }}>
                <p className="listObjectName">
                  {!shoppinglist.name &&
                    shoppinglist.id
                  }
                  {shoppinglist.name}
                </p>
              </Link>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default ShoppingListBrowser;


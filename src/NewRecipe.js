import React, { Component } from 'react';
import update from 'react-addons-update';
import database from './index';
import * as Ingredients from './IngredientHelpers';

var randomize = require('randomatic');
var base64 = require('base-64');

import './App.css';


class NewRecipe extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      effort: null,
      portions: null,
      ingredients: null,
      instructions: null,
      stars: {1: 0, 2: 0, 3: 0, 4: 0},
      foodproducts: null,
      foodproductdb: null,
      foodgroups: null
    }

    this.submitIngredient = this.submitIngredient.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchFoodProducts = this.fetchFoodProducts.bind(this);
    this.fetchFoodGroups = this.fetchFoodGroups.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEffortChange = this.handleEffortChange.bind(this);
    this.handlePortionsChange = this.handlePortionsChange.bind(this);
    this.handleInstructionsChange = this.handleInstructionsChange.bind(this);
  }

  submitIngredient(ingredient) {
    var newIngredient = ingredient[0];
    newIngredient.name = ingredient[0].foodproduct.name;

    if (!this.state.ingredients || this.state.ingredients.length === 0) {
      this.setState(function () {
        return {
          ingredients: [newIngredient]
        } 
      })
    } else {
      var newArray = this.state.ingredients.slice();
      
      for (var i = 0; i < newArray.length; i++) {
        var listIngredient = newArray[i];
        if (listIngredient.name === newIngredient.name) {
          listIngredient.amount = Number(listIngredient.amount) + Number(newIngredient.amount);   
          if (listIngredient.amount > 0) {
            newArray[i] = listIngredient;
          } else if (newArray.length > 1) {
            newArray.splice(i, 1);
          } else {
            newArray = [];       
          }
          break;
        } else if (i === (newArray.length-1)) {
          newArray.push(newIngredient);
          break;
        }
      } 

      const foodproducts = this.state.foodproductdb;
      const foodgroups = this.state.foodgroups;
      newArray.sort(function(a,b) {
        let aval = foodgroups[foodproducts[a.name].foodgroup];
        let bval = foodgroups[foodproducts[b.name].foodgroup];

        if (!aval) aval = Infinity;
        if (!bval) bval = Infinity;

        return ((aval < bval) ? -1 : 
               ((aval > bval) ?  1 : 0));
      })

      this.setState(function () {
        return {
          ingredients: newArray
        }
      })
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    var id = null;
    database.ref('/recipes').once('value').then(function (recipes) {
      while (true) {
        id = base64.encode(randomize('*', 6));
        
        if (!recipes[id]) {
          break;
        }
      }
    }).then(function () {
        var recipe = {};
	    recipe.name = this.state.name;
	    recipe.effort = this.state.effort;
	    recipe.portions = this.state.portions;
	    recipe.stars = {1: 0, 2: 0, 3: 0, 4: 0};

	    database.ref('/recipes/' + id).set(recipe);

	    var list = {};
	    this.state.ingredients.map(function(ingredient) {
	      list[ingredient.name] = ingredient.amount;
	    })
	    database.ref('/recipes/' + id + '/ingredients').set(list);

	    database.ref('/instructions/' + id).set(this.state.instructions);

	    this.props.history.push('/recipes/check?id=' + id);
    }.bind(this));
  }



  handleNameChange(event) {
  	var value = event.target.value;

    this.setState(function(){
      return {
        name: update(this.state.name, {$set: value})
      }
    })
  }

  handleEffortChange(event) {
  	var value = event.target.value;

    this.setState(function(){
      return {
        effort: update(this.state.effort, {$set: value})
      }
    })
  }

  handlePortionsChange(event) {
  	var value = event.target.value;

    this.setState(function(){
      return {
        portions: update(this.state.portions, {$set: value})
      }
    })
  }


  handleInstructionsChange(event) {
  	var value = event.target.value;

    this.setState(function(){
      return {
        instructions: update(this.state.instructions, {$set: value})
      }
    })
  }

  fetchFoodGroups() {
    database.ref('/foodgroups-sorting').once('value').then(function (foodgroups) {
      return foodgroups.val();
    }).then(function (foodgroups) {
      this.setState(function () {
        return {
          foodgroups: foodgroups
        }
      })
    }.bind(this));
  }

  fetchFoodProducts() {
    database.ref('/foodproducts').once('value').then(function (foodproducts) {
      return foodproducts.val();
    }).then(function (foodproducts) {
      var foodproductList = [];
      Object.keys(foodproducts).map(function (key,index) {
        let foodproduct = foodproducts[key];
        foodproduct.name = key;
        foodproductList.push(foodproduct);
      })
      
      const foodgroups = this.state.foodgroups;
      
      foodproductList.sort(function(a,b) {
        return ((foodgroups[foodproducts[a.name].foodgroup] < foodgroups[foodproducts[b.name].foodgroup]) ? -1 : 
                ((foodgroups[foodproducts[a.name].foodgroup] > foodgroups[foodproducts[b.name].foodgroup]) ? 1 : 0));
      })

      this.setState(function () {
        return {
          foodproducts: foodproductList,
          foodproductdb: foodproducts
        }
      })
    }.bind(this));
  }


  componentDidMount() {
    this.fetchFoodGroups();
    this.fetchFoodProducts();
  }



  render() {
    return (
    	<div className = 'recipe-new'>
	    	<h2> Create your own recipe below! </h2>
	    	<div className='row'>
		      <form className='recipe-new-details' onSubmit={this.handleSubmit}>
		        <fieldset className='row'>
		          <legend>{this.state.name}</legend>
		          <div>
			        <p>Name:</p>
			        <input
			          id='name'
			          placeholder=''
			          type='text'
			          autoComplete='off'
			          value={this.state.name}
			          onChange={this.handleNameChange}/>
		         
			        <p>Instructions:</p>
              <textarea
                className='instructions'
                id='instructions'
                placeholder='Give the instructions here...'
                type='text'
                autoComplete='off'
                value={this.state.instructions}
                onChange={this.handleInstructionsChange}
              />
		          
			        <p>Effort it takes (minutes):</p>
			        <input
			          id='effort'
			          placeholder='0'
			          type='number'
			          autoComplete='off'
			          value={this.state.effort}
			          onChange={this.handleEffortChange}/>
		         
			        <p>How many portions?</p>
			        <input
			          id='portions'
			          placeholder='0'
			          type='number'
			          autoComplete='off'
			          value={this.state.portions}
			          onChange={this.handlePortionsChange}/>
		          </div>
		          <button
		            className='button'
		            type='submit'
		            disabled={!this.state.ingredients}>
		          Save recipe
		          </button>
		        </fieldset>
		      </form>

		      <div className='recipe-new-ingredients'>
		        <p>Ingredients</p>
		        {(this.state.ingredients && this.state.foodproductdb) &&
              <Ingredients.IngredientList foodproducts={this.state.foodproductdb} ingredients={this.state.ingredients}/>
			      }
			      {this.state.foodproducts &&
			        <Ingredients.IngredientAdd foodproducts={this.state.foodproducts} onSubmit={this.submitIngredient}/>
			      }
		      </div>
		    </div>
		  </div>
    )
  }
}


export default NewRecipe;

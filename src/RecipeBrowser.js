import React, { Component } from 'react';
import update from 'react-addons-update';
import database from './index';
import Icon from './Icon';

var Stars = require('react-rating');
var randomize = require('randomatic');
var base64 = require('base-64');
var Link = require('react-router-dom').Link;

import './App.css';



class RecipeBrowser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      recipes: null,
      foodproducts: null
    }

    this.submitRecipe = this.submitRecipe.bind(this);
    this.updateRecipes = this.updateRecipes.bind(this);
    this.fetchFoodProducts = this.fetchFoodProducts.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
  }

 
  submitRecipe(recipe,instructions) {
    var id = base64.encode(randomize('*', 8));
    database.ref('/recipes/' + id).set(recipe);
    database.ref('/instructions/' + id).set(instructions);
    this.updateRecipes();
  }


  updateRecipes() {
    database.ref('/recipes').once('value').then(function (recipes) {
      return recipes.val();
    }).then(function (recipes) {
      var recipeList = [];
      Object.keys(recipes).map(function (key,index) {
        let recipe = recipes[key];
        recipe.id = key;
        recipeList.push(recipe);
      })

      this.setState(function () {
        return {
          recipes: recipeList
        }
      })
    }.bind(this));
  }

  fetchFoodProducts() {
    database.ref('/foodproducts').once('value').then(function (foodproducts) {
      return foodproducts.val();
    }).then(function (foodproducts) {
      var foodproductList = [];
      Object.keys(foodproducts).map(function (key,index) {
        let foodproduct = foodproducts[key];
        foodproduct.name = key;
        foodproductList.push(foodproduct);
      })
//------------------- SORT HERE -----------------------------//
      foodproductList.sort(function(a,b) {
        return ((a.foodgroup < b.foodgroup) ? -1 : 
                ((a.foodgroup > b.foodgroup) ? 1 : 0));
      })

      this.setState(function () {
        return {
          foodproducts: foodproductList
        }
      })
    }.bind(this));
  }

  handleSortChange(event) {
    var sortby = event.target.value;
    let fn = function(a,b) {
      return ((a.name < b.name) ? -1 : 
             ((a.name > b.name) ? 1 : 0));
    }
    switch(sortby) {
      case "rating":
        fn = function(a,b) {
            return ((a.stars[3] < b.stars[3]) ? 1 : 
                   ((a.stars[3] > b.stars[3]) ? -1 : 0));
          }
        break;
      case "effort":
        fn = function(a,b) {
            return ((a.effort < b.effort) ? -1 : 
                   ((a.effort > b.effort) ? 1 : 0));
          }
        break;
    }
    var sorted = this.state.recipes.sort(fn);
    this.setState(function () {
      return {
        recipes: sorted
      }
    })
  }

  componentDidMount() {
    this.fetchFoodProducts();
    this.updateRecipes();
  }

  render() {
    return (
      <div className='recipe-list'>        
        <h3>Recipes:</h3>
        {this.state.recipes &&
          <div>
            <SortRecipes onChange={this.handleSortChange} recipes={this.state.recipes}/>
            <AddRecipe match={this.props.match} />
            <SelectRecipe match={this.props.match} recipes={this.state.recipes}/>
          </div>
        }
      </div>
    )
  }
}

function SortRecipes (props) {
  return (
    <div className='recipe-sort'>
      <table>
        <thead>
          <tr>
            <th></th><th>Sort by</th>
          </tr>
        </thead>
        <tbody>
          <tr><td><input type="radio" name="sort" onChange={props.onChange} value="name"/></td><td> Name </td><br/></tr>
          <tr><td><input type="radio" name="sort" onChange={props.onChange} value="rating"/></td><td> Most 3 stars </td><br/></tr>
          <tr><td><input type="radio" name="sort" onChange={props.onChange} value="effort"/></td><td> Effort </td><br/></tr>
        </tbody>
      </table>
    </div>
  )
}

function SelectRecipe (props) {
  return (
    <div className='recipe-select'>
      {props.recipes.map(function (recipe) {
        return (
          <RecipeBox img='dish.svg' recipe={recipe} url={props.match.url}/>
        )})}
    </div>
  )
}

class RecipeBox extends Component {
  getScore(stars) {
    var votes = 0;
    var starcount = 0;

    Object.keys(stars).map(function (key,index) {
      votes += stars[key];
      starcount += (Number(key)*stars[key]);
    })

    if (votes != 0) {
      starcount /= votes;
      var score = starcount.toFixed(1);
    }
    return score;
  }

  render() {
    return (
      <div className='recipe-box'>
        <Link
          className='button'
          to={{
            pathname: this.props.url + '/check',
            search: '?id=' + this.props.recipe.id
          }}>
          <img src={this.props.img} alt=''/>
          <p className="listObjectName">
            {!this.props.recipe.name &&
              this.props.recipe.id
            }
            {this.props.recipe.name}
          </p>
        </Link>
        <p>Effort: {this.props.recipe.effort} min</p>
        <Stars stop={4} initialRate={+this.getScore(this.props.recipe.stars)} readonly={true}
               empty={<Icon src='star_empty.svg' alt='empty star'/>}
               placeholder={<Icon src='star_full.svg' alt='star'/>}
               full={<Icon src='star_full.svg' alt='star'/>}
        />
      </div>
    )
  }
}

function AddRecipe (props) {
  return (
    <div className='recipe-add'>
      <Link
        className='button'
        to={{
          pathname: props.match.url + '/new',
        }}>
        <p className="button-text">
          Add new recipe
        </p>
      </Link>
    </div>
  )
}


export default RecipeBrowser;


import React, { Component } from 'react'

class IngredientAdd extends Component {
  constructor(props) {
    super(props);

    this.state = {
      foodproduct: this.props.foodproducts[0],
      amount: 0
    }

    this.handleFoodProductChange = this.handleFoodProductChange.bind(this);
    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleFoodProductChange(event) {
    var value = event.target.value;
    var product = this.props.foodproducts[value];
    this.setState(function(){
      return {
        foodproduct: product
      }
    })  
  }

  handleAmountChange(event) {
    var value = event.target.value;

    this.setState(function(){
      return {
        amount: value
      }
    })
  }

  handleSubmit(event) {
    event.preventDefault();

    var newIngredient = {};
    newIngredient.foodproduct = this.state.foodproduct;
    newIngredient.amount = this.state.amount;
    this.props.onSubmit([newIngredient]);

    this.setState(function(){
      return {
        amount: 0
      }
    })
  }

  render() {
    return (
      <div className='ingredient-add'>
        <form className='row' onSubmit={this.handleSubmit}>
          <input
            id='amount'
            placeholder='Amount'
            type='number'
            autoComplete='off'
            value={this.state.amount}
            onChange={this.handleAmountChange}/>
          <select id='foodproduct' onChange={this.handleFoodProductChange}>
            {this.props.foodproducts.map(function (foodproduct, index){
              return <option value={index}>{foodproduct.name}</option>;  
            })}
          </select>
          <button
            className='button'
            type='submit'
            disabled={this.state.amount === 0}>
          Add ingredient
          </button>
        </form>
      </div>
    )
  }
}

class RecipeAdd extends Component {
  constructor(props) {
    super(props);

    this.state = {
      recipe: this.props.recipes[0],
      multiplier: 0
    }

    this.handleRecipeChange = this.handleRecipeChange.bind(this);
    this.handleMultiplierChange = this.handleMultiplierChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleRecipeChange(event) {
    var value = event.target.value;
    var recipe = this.props.recipes[value];
    this.setState(function(){
      return {
        recipe: recipe
      }
    })  
  }

  handleMultiplierChange(event) {
    var value = event.target.value;

    this.setState(function(){
      return {
        multiplier: value
      }
    })
  }

  handleSubmit(event) {
    event.preventDefault();

    var newRecipeAdd = {};
    newRecipeAdd.recipe = this.state.recipe;
    newRecipeAdd.multiplier = this.state.multiplier;
    this.props.onSubmit(newRecipeAdd);

    this.setState(function(){
      return {
        multiplier: 0
      }
    })
  }

  render() {
    return (
      <div className='ingredient-add'>
        <form className='row' onSubmit={this.handleSubmit}>
          <input
            id='multiplier'
            placeholder='Multiplier'
            type='number'
            autoComplete='off'
            value={this.state.multiplier}
            onChange={this.handleMultiplierChange}/>
          <select id='recipe' onChange={this.handleRecipeChange}>
            {this.props.recipes.map(function (recipe, index){
              return <option value={index}>{recipe.name}</option>;  
            })}
          </select>
          <button
            className='button'
            type='submit'
            disabled={this.state.multiplier === 0}>
          Add ingredients of recipe
          </button>
        </form>
      </div>
    )
  }
}

function IngredientList (props) {
  var ingredients = props.ingredients.slice();
  ingredients.map(function (ingredient) {
    let measure = props.foodproducts[ingredient.name].measure;
    if (!measure) {
      measure = 'g';
    }
    ingredient.measure = measure;
  })
  return (
    <div className='ingredient-table'>
      <table>
        <thead>
          <tr>
            <th>Amount</th>
            <th>Product</th>
          </tr>
        </thead>
        <tbody>
          {ingredients.map(function (ingredient){
            return <tr><td>{ingredient.amount}{ingredient.measure}</td><td>{ingredient.name}</td></tr>;  
          })}
        </tbody>
      </table>
    </div>
  )
}

function RecipeList (props) {
  return (
    <div className='recipe-table'>
      <table>
        <thead>
          <tr>
            <th>Size</th>
            <th>Recipe</th>
          </tr>
        </thead>
        <tbody>
          {props.recipes.map(function (recipe){
            return <tr><td>{recipe.multiplier} x</td><td>{recipe.recipe.name}</td></tr>;  
          })}
        </tbody>
      </table>
    </div>
  )
}


export default IngredientList;
export {IngredientAdd, IngredientList, RecipeAdd, RecipeList};
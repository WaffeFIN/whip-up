import React, { Component } from 'react';

import './App.css';

import Nav from './Nav';
import Home from './Home';
import ShoppingListBrowser from './ShoppingListBrowser';
import FoodProduct from './FoodProduct';
import RecipeBrowser from './RecipeBrowser';
import Recipe from './Recipe';
import NewRecipe from './NewRecipe';
import ShoppingList from './ShoppingList';

var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;



class App extends Component {
  render() {
    return (
      <Router>
        <div className='container'>
          <Nav />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/list' component={ShoppingListBrowser} />
            <Route path='/list/edit' component={ShoppingList} />
            <Route exact path='/recipes' component={RecipeBrowser}/>
            <Route path='/recipes/check' component={Recipe}/>
            <Route path='/recipes/new' component={NewRecipe}/>
            <Route path='/foodproduct' component={FoodProduct} />
            <Route render={
              function() {
                return <p>Not found</p>
              }
            } />
          </Switch>
          <Footer />
        </div>
      </Router>
    )
  }
}

function Footer(props) {
  return (
    <footer>&copy; 2017 Walter Grönholm & Paul Vuorela </footer>
  )
}


export default App;

import React from 'react';

function Icon (props) {
	var size = props.size;
	if (size === undefined)
		size = 32;
	return (
		<img src={props.src} alt={props.alt} width={size} height={size}/>
	)
}

export default Icon;
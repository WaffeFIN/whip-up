import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

import firebase from 'firebase';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyAeCO5C13MKXauji4KuiwAfXoseeebnM-E",
  authDomain: "whip-up.firebaseapp.com",
  databaseURL: "https://whip-up.firebaseio.com",
  projectId: "whip-up",
  storageBucket: "whip-up.appspot.com",
  messagingSenderId: "540933224909"
};
firebase.initializeApp(config);

var database = firebase.database();

export default database

ReactDOM.render(
	<App />,
	document.getElementById('root')
);

